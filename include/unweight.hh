#pragma once

#include <numeric>
#include <stdexcept>
#include <random>
#include <type_traits>

#include "Bin.hh"

namespace detail {
  template<typename Iterator>
  void to_pos_weight(
    Iterator begin, Iterator end,
    std::true_type // iterate over events
  ) {
    if(begin == end) return;
    const double wt_sum = sum_wt(begin, end);
    const double abs_wt_sum = std::accumulate(
      begin, end, 0.,
      [](double sigma, Event const & ev) {
        return sigma +  std::abs(ev.weights().front());
      }
    );
    const double reweight_factor = wt_sum / abs_wt_sum;
    for(auto ev_it = begin; ev_it != end; ++ev_it) {
      Event & ev = *ev_it;
      ev.weights().front() = reweight_factor*std::abs(ev.weights().front());
    }
  }

  template<typename Iterator>
  void to_pos_weight(
    Iterator begin, Iterator end,
    std::false_type // doesn't iterate over events
  ) {
    for(;begin != end; ++begin) to_pos_weight(*begin);
   }
}

template<typename Iterator>
void to_pos_weight(Iterator begin, Iterator end) {
  using IsEventIt = std::is_convertible<
    typename Iterator::value_type, HepMC::GenEvent
  >;
  return detail::to_pos_weight(begin, end, IsEventIt{});
}

template<template<class, class> class Container, typename T, class Allocator>
void to_pos_weight(Container<T, Allocator> & c) {
  to_pos_weight(begin(c), end(c));
}

template<class T>
void to_pos_weight(Bin<T> & bin) {
  to_pos_weight(bin.entries);
}

namespace detail {
  // pick a random event from the range
  // with chance proportional to the absolute event weight
  template<typename Iterator, class RNG>
  Iterator pick_weighted_random(
    Iterator begin, Iterator end,
    RNG& rng
  ) {
    const double abs_wt_sum = std::accumulate(
      begin, end, 0.,
      [](double sigma, Event const & ev) {
        return sigma +  std::abs(ev.weights().front());
      }
    );
    std::uniform_real_distribution<double> ran{0., abs_wt_sum};
    const double target_abs_wt_sum = ran(rng);
    double cur_abs_wt_sum = 0.;
    for(auto event_it = begin; event_it != end; ++event_it) {
      Event & event = *event_it;
      cur_abs_wt_sum += std::abs(event.weights().front());
      if(cur_abs_wt_sum >= target_abs_wt_sum) return event_it;
    }
    throw std::logic_error{"exceeded weight sum"};
  }
}

template<class T, class RNG>
void unweight(
  std::vector<T> & events,
  RNG & rng,
  const double max_wt
) {
  if(events.empty()) return;
  const double orig_height = sum_wt(events);
  auto begin_removed = std::partition(
    begin(events), end(events),
    [max_wt,&rng](Event const & ev) {
      if(std::abs(ev.weights().front()) >= max_wt) return true;
      std::uniform_real_distribution<double> ran{0., max_wt};
      return ran(rng) < std::abs(ev.weights().front());
    }
  );
  // ensure that we don't remove all events
  if(begin_removed == std::begin(events)) {
    auto survivor = detail::pick_weighted_random(
      std::begin(events), std::end(events), rng
    );
    assert(survivor != std::end(events));
    std::swap(*begin_removed, *survivor);
    ++begin_removed;
  }
  for(auto event_it = begin_removed; event_it != end(events); ++event_it) {
    Event & event = *event_it;
    event.weights().front() = 0.;
  }
  events.erase(begin_removed, end(events));
  for(Event & ev: events) {
    if(std::abs(ev.weights().front()) < max_wt) {
      ev.weights().front() = (ev.weights().front() > 0.)?max_wt:-max_wt;
    }
  }
  const double new_height = sum_wt(events);
  // rescale to ensure that the sum of weights is preserved exactly
  if(new_height != 0.) {
    for(Event & ev: events) {
      ev.weights().front() *= orig_height / new_height;
    }
  }
}

template<class T, class RNG>
void unweight(
  std::vector<Bin<T>> & bins,
  RNG & rng,
  const double max_wt
) {
  for(auto & bin: bins) unweight(bin, rng, max_wt);
}

template<class T, class RNG>
void unweight(Bin<T> & bin, RNG & rng, const double max_wt) {
  unweight(bin.entries, rng, max_wt);
}

template<class Entry, class RNG, class... DistrInfo>
void unweight_by_distr(
  std::vector<Entry> & events,
  const bool pos_weight,
  const double unweight_weight,
  RNG & ran,
  DistrInfo&&... distr
) {
  auto bins = bin_by_attr(events, std::forward<DistrInfo>(distr)...);

  if(pos_weight) to_pos_weight(bins);
  unweight(bins, ran, unweight_weight);
}

template<class Entry, class RNG>
void unweight_by_distr(
  std::vector<Entry> & events,
  std::vector<DistrInfo> const & distr,
  const bool pos_weight,
  const double unweight_weight,
  RNG & ran
) {
  assert(! distr.empty());
  switch(distr.size()) {
  case 1:
    return unweight_by_distr(events, pos_weight, unweight_weight, ran, distr[0]);
  case 2:
    return unweight_by_distr(
      events, pos_weight, unweight_weight, ran,
      distr[0], distr[1]
    );
    // add higher case numbers here if needed
  default:
    throw std::logic_error{
      "Differential unweighting in " + std::to_string(distr.size())
        + " variables not implemented"
    };
  }
}
