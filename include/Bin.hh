#pragma once

#include <cmath>
#include <cassert>
#include <functional>
#include <numeric>
#include <stdexcept>
#include <vector>

#include "HepMC/GenEvent.h"

#include "utility.hh"

template<class T>
struct Bin {
  Bin() = default;
  Bin(double bin_min, double bin_max):
    min{bin_min},
    max{bin_max},
    entries{}
  {}
  Bin(
    double bin_min, double bin_max,
    std::vector<T> entries
  ):
    min{bin_min},
    max{bin_max},
    entries{std::move(entries)}
  {}

  using Entry = T;

  double min;
  double max;
  std::vector<T> entries;
};

template<
  class BinIterator,
  class Entry = typename BinIterator::value_type::Entry
  >
void fill(
  BinIterator begin, BinIterator end,
  double key, Entry & entry
) {
  const auto bin_it = std::lower_bound(
    begin, end, key,
    [](auto const & bin, double key) {
      return bin.max < key;
    }
  );
  if(bin_it == end) {
    throw std::logic_error{"no bin for value " + std::to_string(key)};
  }
  bin_it->entries.emplace_back(entry);
}

template<
  class Iterator,
  class Function,
  class Entry = typename Iterator::value_type
  >
std::vector<Bin<Entry>> gather_by(
  Iterator begin, Iterator end,
  Function f
) {
  if(begin == end) return {};
  auto range = get_range(begin, end, f);
  const size_t nbins = std::sqrt(std::distance(begin, end));
  const double step_size = (range.second - range.first)/nbins;
  std::vector<Bin<Entry>> bins;
  bins.reserve(nbins);
  for(size_t bin = 0; bin < nbins; ++bin) {
    const double min = range.first + bin*step_size;
    assert(min <= range.second);
    bins.emplace_back(min, min + step_size);
  }
  bins.back().max = range.second;

  for(auto event_it = begin; event_it != end; ++event_it) {
    fill(std::begin(bins), std::end(bins), f(*event_it), *event_it);
  }
  return bins;
}

template<
  class Iterator,
  class Transform,
  class Entry = typename Iterator::value_type
  >
std::vector<Bin<Entry>> gather_by_attr(
  std::string const & attr,
  Iterator begin, Iterator end,
  Transform t
) {
  return gather_by(
    begin, end,
    [=](Event const & ev) {
      return t(ev.weights()[attr]);
    }
  );
}

inline
double Log(double x) {
  return std::log(x);
}

template<typename T>
double sum_wt(Bin<T> const & bin) {
  return sum_wt(bin.entries);
}

template<typename T>
double sum_wt2(Bin<T> const & bin) {
  return sum_wt2(bin.entries);
}

namespace detail {

  template<class Entry>
  std::vector<Bin<Entry>> bin_by_attr(
    std::vector<Entry> & events,
    std::string const & distr_name
  ) {
    assert(distr_name != "total");

    return gather_by(
      begin(events), end(events),
      [=](Event const & ev) {
        return ev.weights()[distr_name];
      }
    );
  }


  template<class Entry>
  std::vector<Bin<Entry>> bin_by_log_attr(
    std::vector<Entry> & events,
    std::string const & distr_name
  ) {
    assert(distr_name != "total");

    const auto end_good = std::stable_partition(
      begin(events), end(events),
      [=](Event & event) { return event.weights()[distr_name] > 0.; }
    );

    auto bins = gather_by(
      begin(events), end_good,
      [=](Event const & ev) {
        return std::log(ev.weights()[distr_name]);
      }
    );
    constexpr auto inf = std::numeric_limits<double>::infinity();
    bins.insert(begin(bins), Bin<Entry>{-inf, -inf});
    for(auto it = end_good; it != end(events); ++it) {
      bins.front().entries.emplace_back(*it);
    }
    return bins;
  }

}

template<class Entry>
std::vector<Bin<Entry>> bin_by_ptW_peak(
  std::vector<Entry> & events
);

struct DistrInfo {
  DistrInfo() = default;
  DistrInfo(std::string const & name, const bool log):
    name{name},
    log{log}
  {}

  std::string name;
  bool log;
};

template<class Entry>
std::vector<Bin<Entry>> bin_by_attr(
  std::vector<Entry> & events,
  DistrInfo const & info
) {
  if(info.name == "total") {
    std::vector<Bin<Entry>> result(1);
    for(auto & event: events) {
      result.front().entries.emplace_back(event);
    }
    return result;
  }
  if(info.name == "ptW_peak") {
    return bin_by_ptW_peak(events);
  }
  if(info.log) return detail::bin_by_log_attr(events, info.name);
  return detail::bin_by_attr(events, info.name);
}

template<class Entry, class... Rest>
auto bin_by_attr(
  std::vector<Entry> & events,
  DistrInfo const & info0,
  DistrInfo const & info1,
  Rest&&... r
) {
  using HistNdVec = decltype(bin_by_attr(events, info1, std::forward<Rest>(r)...));
  using HistNd = typename HistNdVec::value_type;

  // bin by first attribute
  auto hist_1d = bin_by_attr(events, info0);

  // split each bin recursively according to remaining attributes
  std::vector<Bin<HistNd>> result;
  for(auto & bin: hist_1d) {
    result.emplace_back(
      bin.min, bin.max,
      bin_by_attr(bin.entries, info1, std::forward<Rest>(r)...)
    );
  }
  return result;
}
