#pragma once

#include <string>
#include <vector>

#include "cxxopts.hpp"

struct Config {
  std::string help_message;
  std::vector<std::string> distributions;
  std::vector<bool> log_distribution;
  double unweight;
  unsigned long seed;
  bool help;
  bool pos_weight;
};

inline
Config get_opt(
  int argc, char** argv
) {
  cxxopts::Options options{
    argv[0],
    "Unweight events"
  };

  options.add_options()
    ("h,help", "show this help message and exit")
    (
      "distributions",
      "HepMC named weights according to which events should be unweighted.",
      cxxopts::value<std::vector<std::string>>()->default_value("total")
    )
    (
      "log-distribution",
      "Use logarithmic scale if unweighting according to a distribution.",
      cxxopts::value<std::vector<bool>>()->default_value("false")
    )
    (
      "pos-weight",
      "Make all weights positive.",
      cxxopts::value<bool>()->default_value("true")
    )
    (
      "seed",
      "Seed for random number generator.",
      cxxopts::value<unsigned long>()->default_value("0")
    )
    (
      "unweight",
      "Target weight for unweighting. 0. means no unweighting.",
      cxxopts::value<double>()->default_value("0.")
    );
  options.custom_help("[OPTIONS...] EVENTFILE OUTFILE");

  auto result = options.parse(argc, argv);
  Config c;
  c.help_message = options.help();
  c.distributions = result["distributions"].as<std::vector<std::string>>();
  c.log_distribution = result["log-distribution"].as<std::vector<bool>>();
  c.unweight = result["unweight"].as<double>();
  c.seed = result["seed"].as<unsigned long>();
  c.help = result["help"].as<bool>();
  c.pos_weight = result["pos-weight"].as<bool>();

  if(!c.help && argc != 3) {
    throw std::invalid_argument{
      "Expected 2 command line arguments, found " + std::to_string(argc) + "\n"
        + options.help()
    };
  }

  if(c.distributions.size() != c.log_distribution.size()) {
    throw std::invalid_argument{
      "Number of distributions (" + std::to_string(c.distributions.size()) + ")"
        " does not match length of log-distribution option ("
        + std::to_string(c.log_distribution.size()) + ")"
    };
  }

  return c;
}
