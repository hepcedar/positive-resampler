#pragma once

#include <optional>
#include <stdexcept>

#include "Bin.hh"

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/WFinder.hh"

namespace detail {

  inline
  Rivet::WFinder make_WFinder() {
      namespace Cuts = Rivet::Cuts;
      using Rivet::GeV;
      constexpr double dR = 0.2;

      Rivet::FinalState fs;
      // the code for this cut differs from the original code in MC_WINC
      // the original returns a `bool`, no idea how that is supposed to work
      const Rivet::Cut lepton_cut =
        Cuts::absetaIn(0.0, 3.5) && Cuts::ptIn(25*GeV, 100000*GeV /* some large number */);
      return {
        fs,
        lepton_cut, Rivet::PID::ELECTRON,
        60.0*GeV, 100.0*GeV, 25.0*GeV, dR
      };
  }

  // dirty hack to get access to Rivet::WFinder's protected members
  class WFinder: public Rivet::WFinder {
  public:
    WFinder(): Rivet::WFinder{make_WFinder()}
    {}

    std::optional<Rivet::FourMomentum> find_W(Rivet::Event const & event) {
      clear(); // may or may not be necessary
      project(event);
      if(bosons().size() == 1) {
        return {bosons().front().momentum()};
      }
      return {};
    }
  };

  template<class Iterator>
  double max_ptW(
    Iterator begin, Iterator end,
    WFinder & wfinder
  ) {
    double max_ptW = 0.;
    for(auto event_it = begin; event_it != end; ++event_it) {
      Event & event = *event_it;
      const auto pW = wfinder.find_W(event);
      if(pW && pW->pT() > max_ptW) max_ptW = pW->pT();
    }
    return max_ptW;
  }

  // Binning taken from rivet MC_WINC
  // + some more to capture *all* events that pass the analysis cuts
  static constexpr double pt_min = 0.0;
  static constexpr double pt_max = 125.0;
  static constexpr size_t num_bins = 25;
  static constexpr double bin_width = (pt_max - pt_min) / num_bins;

  template<class Entry>
  std::vector<Bin<Entry>> ptW_peak_bins(const double max_ptW) {
    std::vector<Bin<Entry>> bins;
    for(double bin_min = 0.; bin_min < max_ptW; bin_min += bin_width) {
      bins.emplace_back(bin_min, bin_min + bin_width);
    }
    return bins;
  }

}

template<class Entry>
std::vector<Bin<Entry>> bin_by_ptW_peak(
  std::vector<Entry> & events
) {
  detail::WFinder wfinder{};
  const double max_ptW = detail::max_ptW(
    std::begin(events), std::end(events), wfinder
  );
  auto bins = detail::ptW_peak_bins<Entry>(max_ptW);
  for(auto & event: events) {
    Event & ev = event;
    const auto pW = wfinder.find_W(ev);
    if(pW) {
      const double ptW = pW->pT();
      fill(std::begin(bins), std::end(bins), ptW, event);
    }
  }
  return bins;
}
