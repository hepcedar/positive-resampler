#pragma once

#include <cmath>
#include <numeric>
#include <memory>
#include <stdexcept>
#include <string>

#include "HepMC/IO_GenEvent.h"

using Event = HepMC::GenEvent;

inline
std::vector<Event> read_events(
  std::string const & filename
) {
  std::vector<Event> events;

  HepMC::IO_GenEvent reader{filename, std::ios::in};
  // this is idiotic, but avoids `new` ...
  Event event{};
  auto * event_ptr = &event;
  while(reader >> event_ptr) {
    events.emplace_back(std::move(*event_ptr));
  }
  return events;
}

template<class Iterator, class Function>
std::pair<double, double> get_range(
  Iterator begin, Iterator end,
  Function f
) {

  if(begin == end) {
    throw std::invalid_argument{"empty event range"};
  }
  const double val = f(*begin);
  auto range = std::make_pair(val, val);
  for(;begin != end; ++begin) {
    const double val = f(*begin);
    if(val < range.first) range.first = val;
    else if(val > range.second) range.second = val;
  }
  return range;
}

inline double sum_wt(Event const & ev) {
  return ev.weights().front();
}

template<class Iterator>
double sum_wt(Iterator begin, Iterator end) {
  return std::accumulate(
    begin, end, 0.,
    [](double sum, auto const & ev) { return sum + sum_wt(ev); }
  );
}

template<template<class, class> class Container, typename T, class Allocator>
double sum_wt(Container<T, Allocator> const & c) {
  return sum_wt(begin(c), end(c));
}

inline double sum_wt2(Event const & ev) {
  return ev.weights().front() * ev.weights().front();
}

template<class Iterator>
double sum_wt2(Iterator begin, Iterator end) {
  return std::accumulate(
    begin, end, 0.,
    [](double sum, auto const & ev) { return sum + sum_wt2(ev); }
  );
}

template<template<class, class> class Container, typename T, class Allocator>
double sum_wt2(Container<T, Allocator> const & c) {
  return sum_wt2(begin(c), end(c));
}
