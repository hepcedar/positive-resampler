# Positive Resampler

A Positive Resampler for Monte Carlo Events with Negative Weights.

The positive resampler turns negative event weights into positive
ones, exactly preserving a chosen distribution. For details of the
method see [arXiv:2005.09375](https://arxiv.org/abs/2005.09375).

## Installation

Compiling the resampler requires

* [Rivet](https://rivet.hepforge.org/)
* A compiler supporting the C++17 standard, for instance g++ version 7
  or later.

To compile the program `pos-resampler` run

```
   make
```

## Usage

The basic usage of the positive resampler program is

```
   pos-resample INFILE OUTFILE
```

which reads an event file `INFILE` in HepMC format and writes the
events to `OUTFILE` after positive resampling according to the total
cross section. Possible options are shown with

```
   pos-resample --help
```

### Resampling according to distributions

To specify a distribution d/dt1 d/dt2 ... σ that defines the
resampling factors use

```
   pos-resample --distributions=t1[,t2,...] INFILE OUTFILE
```

Bins are chosen automatically such that there are \sqrt{n} bins of
equal size for n input events. The values of `t1, t2, ...` are taken
from the weights with the same name in the input event records,
i.e. each input event has to come with named weights `t1, t2, ...`
that specify the value of the respective parameter.

There are two special arguments to `distributions`. `total` indicates
that resampling should be performed according to the total cross
section and `ptW_peak` reproduces the corresponding histogram in the
`MC_WINC` Rivet analysis.

To consider a logarithmic distribution dσ/d\log(t) use

```
   pos-resample --distributions=t --log-distribution=true INFILE OUTFILE
```

For higher-dimensional distributions `--log-distribution` accepts a
list of `true` and `false` values.

### Partial Unweighting

Partial unweighting can be enabled with the `--unweight` flag, where
the argument specifies the event weight below which unweighting is
performed. To ensure statistical independence use the `--seed` option
to set an initial state for the pseudo random number generator.

```
   pos-resample --unweight --distributions=t1[,t2,...] INFILE OUTFILE
```
