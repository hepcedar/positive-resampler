#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <random>
#include <stdexcept>
#include <string>
#include <utility>

#include "options.hh"
#include "Bin.hh"
#include "ptW_bins.hh"
#include "utility.hh"
#include "unweight.hh"

void run_main(int argc, char** argv) {
  Config c = get_opt(argc, argv);
  if(c.help) {
    std::cout << c.help_message << '\n';
    return;
  }

  auto events = read_events(argv[1]);
  const double orig_sum_wt = sum_wt(events);
  const double orig_sum_wt2 = sum_wt2(events);
  assert(events.back().cross_section());
  const HepMC::GenCrossSection orig_xs = *events.back().cross_section();

  std::mt19937 ran;
  ran.seed(c.seed);

  std::vector<DistrInfo> distr;
  for(size_t i = 0; i < c.distributions.size(); ++i) {
    distr.emplace_back(c.distributions[i], c.log_distribution[i]);
  }

  // in the following we always operate on references to the original events
  std::vector<std::reference_wrapper<Event>> event_refs;
  for(auto & event: events) event_refs.emplace_back(std::ref(event));
  unweight_by_distr(event_refs, distr, c.pos_weight, c.unweight, ran);

  const double final_sum_wt = sum_wt(events);
  const double final_sum_wt2 = sum_wt2(events);

  HepMC::GenCrossSection final_xs;
  final_xs.set_cross_section(
    final_sum_wt/orig_sum_wt*orig_xs.cross_section(),
    std::sqrt(final_sum_wt2/orig_sum_wt2)*orig_xs.cross_section_error()
  );
  HepMC::IO_GenEvent writer{argv[2], std::ios::out};
  for(Event & ev: events) {
    if(ev.weights().front() == 0.) continue;
    ev.set_cross_section(final_xs);
    auto ev_ptr = &ev;
    writer << ev_ptr;
  }
}

int main(int argc, char** argv) {
  try {
    run_main(argc, argv);
  } catch(std::exception const & ex) {
    std::cerr << "Error:\n  " << ex.what() << "\n\n";
    return EXIT_FAILURE;
  }
}
