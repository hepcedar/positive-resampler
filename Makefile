CXXFLAGS+=-Iinclude -O2 -g --std=c++17 -Wall -Wextra $(shell rivet-config --cppflags)
LDFLAGS+=$(shell rivet-config --ldflags)

ifeq (3,${RIVET_MAJOR_VERSION})
LDLIBS+=-lHepMC $(shell rivet-config --libs)
else
LDLIBS+=-lHepMC -lHepMCfio $(shell rivet-config --libs)
endif

HEADERS=$(wildcard include/*.hh include/*.hpp)

pos-resampler: pos-resampler.cc $(HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ $< $(LDFLAGS) $(LDLIBS)
